from django import forms
from django.core.validators import ValidationError

from . import util


# def validateForUnique(title):
#     if title in util.list_entries():
#         raise ValidationError("The page with the same title already exists!")


class PageForm(forms.Form):
    title = forms.CharField(label="Title")
    content = forms.CharField(label="Content", widget=forms.Textarea(attrs={"style": "height: 400px;"}))


