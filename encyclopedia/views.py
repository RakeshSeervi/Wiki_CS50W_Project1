from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django import forms
from .forms import PageForm
from . import util
from markdown2 import Markdown
import random

markdowner = Markdown()


def index(request):
    return render(request, "encyclopedia/index.html", {
        "title": "",
        "heading": "All Pages",
        "entries": util.list_entries()
    })


def page(request, title):
    content = util.get_entry(title)
    if content:
        return render(request, "encyclopedia/page.html", {
            "content": markdowner.convert(content),
            "title": title
        })
    else:
        return render(request, "encyclopedia/error.html", {
            "message": "Requested page not found!",
            "code": 404
        })


def search(request):
    if request.method == "POST":
        data = request.POST

        content = util.get_entry(data['q'])
        if content:
            return HttpResponseRedirect(reverse('page', args=[data['q']]))
        else:
            entries = util.list_entries()
            found = []
            key = data['q'].lower()
            for entry in entries:
                if key in entry.lower():
                    found.append(entry)

            if found:
                return render(request, "encyclopedia/index.html", {
                    "title": "Search Results",
                    "heading": "Search Results",
                    "entries": found
                })

    return render(request, "encyclopedia/error.html", {
        "message": "Requested page not found!",
        "code": 404
    })


def newPage(request):
    if request.method == "POST":
        form = PageForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            content = form.cleaned_data['content']

            if title in util.list_entries():
                return render(request, "encyclopedia/error.html", {
                    "code": "",
                    "message": "Page already exists!"
                })

            util.save_entry(title, content)
            return HttpResponseRedirect(reverse("page", args=[title]))
        else:
            return render(request, "encyclopedia/newPage.html", {"form": form})

    else:
        return render(request, "encyclopedia/newPage.html", {"form": PageForm()})


def editPage(request, title):
    if request.method == "GET":
        form = PageForm({'title': title, 'content': util.get_entry(title)})
        form.fields['title'].widget = forms.HiddenInput()
        form.fields['title'].validators = []
        print(form)
        return render(request, "encyclopedia/newPage.html", {
            "form": form
        })
    else:
        form = PageForm(request.POST)
        print(form.is_valid())
        print(form.errors)
        if form.is_valid():
            content = form.cleaned_data['content']

            util.save_entry(title, content)
            return HttpResponseRedirect(reverse("page", args=[title]))
        else:
            return HttpResponseRedirect(reverse("editPage", args=[title]))


def randomPage(request):
    entries = util.list_entries()
    randomPageIndex = random.randrange(len(entries))
    return HttpResponseRedirect(reverse("page", args=[entries[randomPageIndex]]))
