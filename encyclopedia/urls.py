from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("wiki/<str:title>", views.page, name="page"),
    path("search", views.search, name="search"),
    path("new", views.newPage, name="newPage"),
    path("<str:title>/edit", views.editPage, name="editPage"),
    path("random", views.randomPage, name="randomPage")
]
